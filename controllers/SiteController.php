<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuarios;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    
    public function actionPrueba() {
        $datos= "<script>alert('1');</script>";
        
        return $this->render("prueba",[
            "datos" => $datos,
        ]);
    }
    
    public function actionFormulario() {
        if($datos=Yii::$app->request->post()){
            return $this->render("prueba",[
                "datos"=> $datos["usuario"]
            ]);
        }else{
            return $this->render("formulario");
        }
    }
    
    public function actionUsuarios() {
        
        $model = new Usuarios();
        if($model->load(Yii::$app->request->post())){
            return $this->render('mostrarUsuarios', ['model' => $model]);
        }else{
            return $this->render('formularioUsuarios', ['model' => $model]);
        }
    }
    
    public function actionFormulariogii()
    {
        $model = new \app\models\Usuarios();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('formularioGii', [
            'model' => $model,
        ]);
    }
}
